# Server-side files
These are server-side files that are used to start the server and have no user interface. You must use the client to play the game.